package au.com.caltex.flagship.dev

import android.app.Application
import android.util.Log
import com.adobe.marketing.mobile.*
import com.google.firebase.FirebaseApp
import com.google.firebase.iid.FirebaseInstanceId
import java.lang.Exception

class AdobePOCApplication: Application() {
    override fun onCreate() {
        super.onCreate()
        FirebaseApp.initializeApp(this)
        configureAdobePlugins()
    }

    private fun configureAdobePlugins() {


        MobileCore.setApplication(this)
        MobileCore.setLogLevel(LoggingMode.VERBOSE)

        try {
            Lifecycle.registerExtension()
            Places.registerExtension()
            PlacesMonitor.registerExtension()
            Campaign.registerExtension()
            Identity.registerExtension()
            Signal.registerExtension()
            UserProfile.registerExtension()

            MobileCore.start {
                MobileCore.configureWithAppID("3e86ca69802a/4775e9981d53/launch-e33a55794e7a-staging")
                MobileCore.lifecycleStart(null)
            }

            FirebaseInstanceId.getInstance().instanceId.addOnSuccessListener {
                val newToken = it.token
                Log.d("AdobePOCApplication", newToken)
                MobileCore.setPushIdentifier(newToken)
            }
        } catch (e: Exception) {
            Log.d("AdobePOCApplication", e.toString())
        }
    }
}