package au.com.caltex.flagship.dev

import android.app.Activity
import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.adobe.marketing.mobile.MobileCore

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        MobileCore.collectPii(mutableMapOf("email" to "testcaltex1@gmail.com"))

        application.registerActivityLifecycleCallbacks(object : Application.ActivityLifecycleCallbacks {
            override fun onActivityResumed(activity: Activity?) {

            }

            override fun onActivityPaused(activity: Activity?) {
                //MobileCore.lifecyclePause()
            }

            override fun onActivityStarted(activity: Activity?) { }
            override fun onActivityDestroyed(activity: Activity?) { }
            override fun onActivitySaveInstanceState(activity: Activity?, outState: Bundle?) { }
            override fun onActivityStopped(activity: Activity?) { }
            override fun onActivityCreated(activity: Activity?, savedInstanceState: Bundle?) { }
        })

    }
}
